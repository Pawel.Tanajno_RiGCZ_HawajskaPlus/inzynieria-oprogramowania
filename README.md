# Skąpiec-Seeker
##### Arkadiusz Pajor & Kamil Szczeszek & Karol Spórna 

## Jak uruchomić program?

#### Wymagania:
1. Python 3.7+.
2. Pipenv ([instrukcja instalacji](https://docs.python-guide.org/dev/virtualenvs/)). 

#### Instrukcja uruchomienia:
1. Pobierz repozytorium.
2. Przejdź do katalogu głównego.
3. Wpisz komendę **pipenv install Pipefile**. Poczekaj aż proces instalacji paczek się zakończy.
4. Wpisz komendę **pipenv shell**.
4. Wpisz komendę **export FLASK_APP=application/view/app.py** jeśli pracujesz na Linuksie lub **set FLASK_APP=application/view/app.py** jeśli na Windowsie lub 
przejdź do katalogu **application/view/**.
5. Wpisz komendę **flask run**.
6. Serwer powinien się uruchomić.

#### Jak korzystać z formularza?
1. Aby uruchomić formularz zakupowy, po uruchomieniu serwera połącz się z nim poprzez przeglądarkę na adresie loopback i porcie 5000 (127.0.0.1:5000)
2. Wprowadź nazwę produktu dostępnego na portalu skąpiec.pl
3. Wprowadź interesujący Cię zakres cen, minimalną ocenę sprzedawcy oraz liczbę sztuk jaką chcesz zakupić.
4. Opcjonalnie wprowadź link do konkretnej oferty z portalu skąpiec.pl (nie zwalnia to z wypełnienia pozostałych pól!)
5. Klikając "Dodaj do zamówienia" poszerzasz swoją listę zakupów o dany przedmiot.
6. Możesz dowolnie dodawać i usuwać przedmioty z listy zakupów, jeśli jest ona niedłuższa, niż 5 elementów.
7. Klikając "Kup" uruchomisz algorytm wyszukujący specjalnie dla Ciebie trzy oferty. Może to potrwać kilkanaście sekund w zależności od liczby produktów.
8. Gdy wyświetli się formularz z produktami, aby przejść do poszczególnych sklepów po prostu kliknij na zdjęcie produktu.
