from flask import Flask, render_template, session, request, redirect
from application.scraper.skapiec import Skapiec
import os

app = Flask(__name__)
app.secret_key = os.urandom(24)


@app.route('/', methods=['POST', 'GET'])
def index():
    """
    Here we do all stuff in main form, which is reading-out client's order details and rendering shopping list form.
    """
    if request.method == 'POST':
        shopping_list = session.get('shopping_list', {})
        product_name = request.form['object']
        input_price_min = request.form['price_min']
        input_price_max = request.form['price_max']
        price_min = int(input_price_min) if input_price_min else 0
        price_max = int(input_price_max) if input_price_max else 0
        if price_max < price_min:
            return redirect('/')
        try:
            score = float(request.form['score'])
        except ValueError:
            score = 4.0
        input_number_of_pieces = request.form['number_of_pieces']
        number_of_pieces = int(input_number_of_pieces) if input_number_of_pieces else 0
        try:
            url = request.form['url']
        except NameError:
            url = ''
        shopping_list[product_name] = {"min_price": price_min, "max_price": price_max, "min_rating": score, "count": number_of_pieces, "optional_url": url}
        session['shopping_list'] = shopping_list
        return redirect('/')
    else:
        return render_template('wishes_list.html', shopping_list=session.get('shopping_list', {}))


@app.route('/delete/<string:item>')
def delete(item):
    """
    Here we remove one element from shopping list and strictly redirect to main form
    """
    shopping_list = session['shopping_list']
    try:
        del shopping_list[item]
    except KeyError:
        pass
    session['shopping_list'] = shopping_list
    return redirect('/')


@app.route('/result')
def result():
    """
    This place is connection point between forms and products selection algorithm.
    Here we pass client's order list and get ready offers sets.
    Finally we render our second form that shows three offers with all the details.
    """
    try:
        shopping_list = session.get('shopping_list', {})
        session['shopping_list'] = {}
        min_number_of_ratings = 50
        skapiec = Skapiec(shopping_list, min_number_of_ratings)
        offers = skapiec.best_offers(3)
        notifications = skapiec.frontend_information_package
        if not offers:
            return render_template('no_matches_error.html')
        else:
            return render_template('offers.html', offers=offers, notifications=notifications)
    except ConnectionError:
        return render_template('connections_number_exceeded_error.html')
    except Exception as e:
        if hasattr(e, 'message'):
            print(e.message)
        else:
            print(e)
        return render_template('basic_error.html')


if __name__ == "__main__":
    app.run(debug=True)