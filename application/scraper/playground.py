from application.scraper.skapiec import Skapiec
from application.scraper.offer import Offer
import time
start = time.time()
# skapiec = Skapiec({"XD": {'count': 15, 'min_price': 400, 'max_price': 10000, 'optional_url':'https://www.skapiec.pl/site/cat/56/comp/83735740'},
#                    "Lenovo Legion Y540 (81SY007CPB)": {'count': 3, 'min_price': 400, 'max_price': 10000},
#                    "NotAProduct": {'count': 1}},  50)
skapiec = Skapiec({"Samsung Galaxy A10 32GB Dual Sim Czarny": {'count': 1, 'min_price': 100, 'max_price': 200000},
                   "mleko": {'count': 10, 'min_price': 700, 'max_price': 6000},
                   "piwo": {'count': 100, 'min_price': 1, 'max_price': 3}},  5)

best = skapiec.best_offers(3)
print("EXECUTION TIME: ", time.time()-start)
for offers in best:
    for offer in offers.offer_combination:
        print(offer)
    print("TOTAL PRICE: ", offers.products_price)
    print("DELIVERY PRICE: ", offers.delivery_price)
    print("MEDIUM RATING: ", offers.medium_rating)
    print("\n--------------------\n")
print(skapiec.frontend_information_package)
