import application.scraper.helpers as helpers
import re
import pprint
import time
import logging
import random
from bs4.element import Tag
from typing import Dict, List, Union
from application.scraper.offer import Offer, OffersCombination

logging.basicConfig(format="%(levelname)s:\t%(message)s", level=logging.INFO)


class Skapiec:
    """
    Skapiec is capable to calculate selected amount of best sets of the given products.
    """

    def __init__(self, products: Dict[str, Dict[str, Union[float, int, str]]], min_opinions: int = 50):
        """
        :param products: dictionary containing products with their attributes - count, minimum and maximum price
        :param min_opinions: minimum number of opinions about shops
        """
        self.frontend_information_package = []
        self.products = products
        self.min_opinions = min_opinions
        self.products_names = list(products.keys())
        self.products_offers = self._get_all_offers()
        self.offers_combinations = OffersCombination.get_all_offers_combinations(self.products_offers)

    def _get_all_offers(self):
        product_offers = {}
        start = time.time()
        valid_products = []
        for product_name in self.products_names:

            if self.products[product_name].get('optional_url'):
                soup = helpers.get_soup(self.products[product_name]['optional_url'])
            else:
                soup = helpers.get_product_soup(product_name)

            # check whether a product has been found
            if soup.find(class_="message only-header info"):
                msg = f"nie odnaleziono produktu '{product_name}'"
                self.frontend_information_package.append(msg)
                logging.warning(msg)
                continue

            # check whether a product name is ambiguous
            if soup.find(class_="icon-view-list"):
                msg = f"nazwa produktu '{product_name}' jest niejednoznaczna, więc produkt został " \
                    f"wybrany losowo z wszystkich możliwych"
                self.frontend_information_package.append(msg)
                logging.warning(msg)
                all_similar_products = soup.find_all(id=re.compile(r"component-\d+"))
                all_products = []
                for product in all_similar_products:
                    product_price = helpers.parse_price(product.find(class_="price gtm_sor_price").text)[0]
                    if self.products[product_name].get('min_price') <= product_price <= self.products[product_name].get('max_price'):
                        all_products.append(product)
                if len(all_products):
                    product = random.sample(all_products, 1)[0]
                elif len(all_similar_products):
                    product = random.sample(all_similar_products, 1)[0]
                else:
                    continue
                soup = helpers.get_soup("https://www.skapiec.pl" + product.find("a").get("href"))

            product_true_name = soup.find(class_='header-content').find("h1").text.strip()
            valid_products.append(product_name)

            promo_offers = soup.find(class_='offers-list promo js')
            if promo_offers:
                promo_offers = promo_offers.find_all(attrs={'data-dealer-id': re.compile(r"\d+")})
                if type(promo_offers) == Tag:
                    promo_offers = [promo_offers]
            else:
                promo_offers = []
            rest_offers = soup.find(class_='offers-list all js')
            if rest_offers:
                rest_offers = rest_offers.find_all(attrs={'data-dealer-id': re.compile(r"\d+")})
                if type(rest_offers) == Tag:
                    rest_offers = [rest_offers]
            else:
                rest_offers = []

            product_offers[product_name] = [Offer(product_true_name,
                                                  offer,
                                                  self.products[product_name]['count'],
                                                  self.products[product_name]['min_price'],
                                                  self.products[product_name]['max_price'],
                                                  self.min_opinions,
                                                  self.products[product_name].get('min_rating', 4.0),
                                                  ctx=self.frontend_information_package)
                                            for offer in (promo_offers + rest_offers)]

        final_products_list = []
        final_product_offers = {}
        for product_name in valid_products:
            final_product_offers[product_name] = []
            for offer in product_offers[product_name]:
                if offer.reliable_shop and offer.in_range:
                    final_product_offers[product_name].append(offer)
            if not final_product_offers[product_name]:
                for offer in product_offers[product_name]:
                    if offer.reliable_shop:
                        final_product_offers[product_name].append(offer)
                if final_product_offers[product_name]:
                    msg = f"nie udało sie znaleźć produktu '{product_name}' dla podanego zakresu cenowego, więc " \
                        f"to kryterium zostało pominięte"
                    self.frontend_information_package.append(msg)
                    logging.warning(msg)
                    final_products_list.append(product_name)
                else:
                    msg = f"produkt '{product_name}', nie spełnia kryteriów"
                    self.frontend_information_package.append(msg)
                    logging.warning(msg)
                    final_product_offers.pop(product_name)
            else:
                final_products_list.append(product_name)

        for i in range(6):
            for product_name in final_products_list:
                for offer in final_product_offers[product_name]:
                    if time.time() - start >= 14.5:
                        msg = "przekroczono maksymalny czas zbierania danych, więc informacje mogą być niekompletne"
                        self.frontend_information_package.append(msg)
                        logging.warning(msg)
                        return product_offers
                    offer.add_delivery_costs(i)

        logging.info("summarized requests and html processing time: {}".format(time.time() - start))
        return final_product_offers

    def best_offers(self, limit: int) -> List[OffersCombination]:
        """
        Calculates and returns the best offers sets with the given criteria (in object constructor).
        :param limit: The number that indicates an amount of returned offers sets
        :return: The best offers sets.
        """
        all_offers = [OffersCombination(offers) for offers in self.offers_combinations]
        sorted_results = sorted(all_offers, key=lambda offers: (offers.total_price, 5 - offers.medium_rating))
        return sorted_results[:limit]

    def __str__(self):
        final_string = ""
        for product in self.products_offers:
            final_string += f"PRODUCT NAME: {product}\n" + "-" * (14 + len(product)) + "\n"
            for shop_product in self.products_offers[product]:
                final_string += f"" \
                    f"SHOP:\t\t\t{shop_product.shop_name}\n" \
                    f"PRICE:\t\t\t{shop_product.price}\n" \
                    f"RATING:\t\t\t{shop_product.rating}\n" \
                    f"OPINIONS:\t\t{shop_product.no_opinions}\n" \
                    f"DELIVERY COSTS:\n" \
                    f"{pprint.pformat(shop_product.delivery_costs, indent=1)}" \
                    f"\n" \
                    f"CHEAPEST:\t\t{shop_product.get_cheapest_delivery_cost()}\n" \
                    f"*****\n\n"
        return final_string
