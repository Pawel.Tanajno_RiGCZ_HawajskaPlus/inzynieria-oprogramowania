import application.scraper.helpers as helpers
import pprint
import logging
import re
from typing import List, Dict

logging.basicConfig(format="%(levelname)s:\t%(message)s", level=logging.INFO)


class Offer:
    def __init__(self,
                 product_name: str,
                 soup: helpers.BeautifulSoup,
                 count: int,
                 min_price: int,
                 max_price: int,
                 min_opinions: int,
                 min_rating: float,
                 ctx: list = None):
        self.product_name = product_name
        self.soup = soup
        self.count = count

        self.min_price = min_price
        self.max_price = max_price
        self.price = self._extract_price()
        self.in_range = True if self.min_price <= self.price <= self.max_price else False
        self.total_price = self.count * self.price

        self.min_opinions = min_opinions
        self.min_rating = min_rating
        self.no_opinions = self._extract_opinions()
        self.rating = self._extract_rating()
        self.reliable_shop = False if self.min_opinions > self.no_opinions or self.min_rating > self.rating else True

        self.shop_name = self._extract_shop_name()
        self.delivery_urls = self._extract_delivery_urls()

        self.delivery_costs = {}

        self.link_to_shop = self._extract_link_to_shop()
        self.link_to_shop_logo = self._extract_link_to_shop_logo()
        self.link_to_product_image = self._extract_link_to_product_image()

        self.ctx = [] if ctx is None else ctx

    @helpers.return_any(10e10, "product's price")
    def _extract_price(self):
        price = self.soup.find(class_="price gtm_or_price").text
        price, pln = helpers.parse_price(price)
        if not pln:
            msg = f"koszt dostawy produktu '{self.product_name}' ze sklepu '{self.shop_name}' " \
                f"został zdefiniowany w innej walucie niż PLN"
            logging.warning(msg)
            self.ctx.append(msg)
        return price

    @helpers.return_any(0, "shop's opinions count")
    def _extract_opinions(self):
        opinions = self.soup.find(class_="counter").text
        return int(opinions)

    @helpers.return_any(0, "rating of the shop")
    def _extract_rating(self):
        row = self.soup.find(class_=re.compile(r"offer-row content-editor gtm_or_bs BadgeAvailability .*"))
        rating = row.get("data-shop-rating")
        return float(rating)

    @helpers.return_any("SHOP NAME NOT FOUND", "name of the shop")
    def _extract_shop_name(self):
        shop = self.soup.find(class_="section-shop-logo")
        shop_name = shop.find("img").get("alt")
        return shop_name

    @helpers.return_any("", "url to the shop's logo")
    def _extract_link_to_shop_logo(self):
        shop = self.soup.find(class_="section-shop-logo")
        shop_logo = shop.find("img").get("src")
        return "http:" + shop_logo

    @helpers.return_any("", "url to the product's image")
    def _extract_link_to_product_image(self):
        image = self.soup.find(class_="section-additional-image")
        if image:
            return "http:" + image.find("img").get("src")
        return ""

    @helpers.return_any("", "url to the shop")
    def _extract_link_to_shop(self):
        return "http://skapiec.pl" + self.soup.find("a").get("href")

    @helpers.return_any([], "urls to the product's deliveries")
    def _extract_delivery_urls(self):
        delivery_cost_options = self.soup.find(class_="section-price-cost")
        if delivery_cost_options.find(class_="delivery-cost free-delivery badge gtm_bdg_fd"):
            return []
        delivery_link = delivery_cost_options.find("a").get("href")
        return ["http://skapiec.pl" + delivery_link + f"&t={i}" for i in range(1, 6)]

    def add_delivery_costs(self, url_number):
        try:
            extended_url = self.delivery_urls[url_number]
        except IndexError:
            return
        delivery_soup = helpers.get_soup(extended_url)

        delivery_type = delivery_soup.find(class_="active")
        if not delivery_type:
            return
        delivery_type = delivery_type.text
        payment_types = delivery_soup.find_all(class_="even") + delivery_soup.find_all(class_="odd")

        self.delivery_costs[delivery_type] = {}
        for payment in payment_types:
            payment_type = payment.find("td").text.strip().replace(" ", "")
            cost = payment.find("b").text.strip().replace(" ", "")
            self.delivery_costs[delivery_type][payment_type], pln = helpers.parse_price(cost)
            if not pln:
                msg = f"koszt dostawy produktu '{self.product_name}' ze sklepu '{self.shop_name}' " \
                    f"został zdefiniowany w innej walucie niż PLN"
                logging.warning(msg)
                self.ctx.append(msg)
        if not self.delivery_costs[delivery_type]:
            self.delivery_costs.pop(delivery_type)

    def get_cheapest_delivery_cost(self):
        if self.delivery_costs:
            delivery_costs = self.delivery_costs.copy()
            if "Odbiór osobisty" in delivery_costs:
                delivery_costs.pop("Odbiór osobisty")
            min_cost = float("inf")
            for delivery_type in delivery_costs:
                try:
                    cost = min(delivery_costs[delivery_type].values())
                    if cost < min_cost:
                        min_cost = cost
                except ValueError:
                    pass
            if min_cost == float("inf"):
                msg = f"nie odnaleziono kosztu dostawy dla produktu '{self.product_name}' ze sklepu '{self.shop_name}'"
                self.ctx.append(msg)
                logging.warning(msg)
                return 0
            return min_cost
        return 0

    def __str__(self):
        return f"" \
            f"NAME:\t\t\t{self.product_name}\n" \
            f"SHOP:\t\t\t{self.shop_name}\n" \
            f"PRICE:\t\t\t{self.price}\n" \
            f"COUNT:\t\t\t{self.count}\n" \
            f"TOTAL:\t\t\t{self.count * self.price}\n" \
            f"RATING:\t\t\t{self.rating}\n" \
            f"OPINIONS:\t\t{self.no_opinions}\n" \
            f"DELIVERY COSTS:\n" \
            f"{pprint.pformat(self.delivery_costs, indent=1)}" \
            f"\n" \
            f"CHEAPEST:\t\t{self.get_cheapest_delivery_cost()}\n" \
            f"\n*****\n"


class OffersCombination:
    def __init__(self, offer_combination: List[Offer]):
        self.offer_combination = offer_combination
        self.products_price, self.delivery_price = self._get_combination_price()
        self.total_price = float("{0:.2f}".format(self.products_price+self.delivery_price))
        self.medium_rating = sum(offer.rating for offer in self.offer_combination)/len(self.offer_combination)

    def _get_combination_price(self):
        delivery_prices = {}
        products_price = 0
        for offer in self.offer_combination:
            products_price += offer.total_price
            offer_cheapest_delivery = offer.get_cheapest_delivery_cost()
            if offer.shop_name not in delivery_prices:
                delivery_prices[offer.shop_name] = offer_cheapest_delivery
            elif delivery_prices[offer.shop_name] < offer_cheapest_delivery:
                delivery_prices[offer.shop_name] = offer_cheapest_delivery
        delivery_price = sum(delivery_prices.values())
        return float("{0:.2f}".format(products_price)), float("{0:.2f}".format(delivery_price))

    @staticmethod
    @helpers.return_any([])
    def get_all_offers_combinations(products_offers: Dict[str, List[Offer]]):
        product_name = list(products_offers.keys())[0]
        if len(products_offers) == 1:
            return [[product] for product in products_offers[product_name]]
        offers = []
        rest_of_offers = products_offers.copy()
        rest_of_offers.pop(product_name)
        rest_of_combinations = OffersCombination.get_all_offers_combinations(rest_of_offers)
        for former_product_offer in products_offers[product_name]:
            for latter_product_offer in rest_of_combinations:
                offers.append([former_product_offer, *latter_product_offer])
        return offers
