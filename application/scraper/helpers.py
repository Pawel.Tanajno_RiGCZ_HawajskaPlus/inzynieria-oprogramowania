import urllib.parse
import requests
import re
import logging
from bs4 import BeautifulSoup
from functools import wraps

logging.basicConfig(format="%(levelname)s:\t%(message)s", level=logging.INFO)


def parse_product_name_to_url(product_name: str) -> str:
    return urllib.parse.quote_plus(product_name.strip())


def get_product_soup(product_name: str) -> BeautifulSoup:
    base_url = "https://www.skapiec.pl/site/szukaj/?szukaj="
    url = base_url + parse_product_name_to_url(product_name)
    return get_soup(url)


def get_soup(url: str) -> BeautifulSoup:
    try:
        response = requests.get(url).text
        logging.info(f"successful response from {url}")
        return BeautifulSoup(response, 'lxml')
    except ConnectionError:
        logging.error(f"failed to connect with {url}")
        return BeautifulSoup("")


def parse_price(price: str):
    price_components = re.search(re.compile(r"([\d ]+)[,.](\d+)(\D+)"), price)
    base = price_components[1].replace(" ", "")
    rest = price_components[2]
    currency = price_components[3].replace(" ", "")
    pln = True if currency == "zł" else False
    return float(base) + float(rest) / 100, pln


def return_any(value_to_return, parameter=None):
    def resolve(operation):
        @wraps(operation)
        def wrapped(*args, **kwargs):
            try:
                return operation(*args, **kwargs)
            except (TypeError, AttributeError, IndexError) as e:
                if parameter:
                    logging.warning(f"parameter '{parameter}' of some product has not been found!")
                return value_to_return
        return wrapped
    return resolve
